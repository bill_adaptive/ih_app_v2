export interface ModuleNavLink {
  path: string;
  label: string;
}

export interface ModuleNavConfig {
  name: string;
  navLinks: ModuleNavLink[];
}
