import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { ReportBuilderModule } from '../../projects/report-builder/src/app/app.module';
import { FormBuilderModule } from '../../projects/form-builder/src/app/app.module';
import { LandingComponent } from './core/landing/landing.component';
import { BaseViewComponent } from './core/base-view/base-view.component';

export const routingConfiguration: ExtraOptions = {
  paramsInheritanceStrategy: 'always',
};

const routes: Routes = [
  {
    path: '',
    component: BaseViewComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'home' },
      {
        path: 'home',
        component: LandingComponent,
      },
      { path: '**', pathMatch: 'full', redirectTo: 'home' },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, routingConfiguration),
    FormBuilderModule.forRoot(),
    ReportBuilderModule.forRoot(),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
