import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';

import { PageService } from '../page.service';
import { ResourceService } from '../resource.service';

@Component({
  selector: 'app-base-view',
  templateUrl: './base-view.component.html',
  styleUrls: ['./base-view.component.scss'],
})
export class BaseViewComponent implements OnInit {

  appTitle$ = 'Loading...';
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay(),
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private pageService: PageService,
    private resourceService: ResourceService,
    private router: Router,
  ) {
    router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        if (ev.urlAfterRedirects === '/') {
          window.location.href = '/home';
        }
      }
    });
  }

  ngOnInit(): void {
    this.appTitle$ = this.pageService.appTitle || this.pageService.appName;
  }
}
