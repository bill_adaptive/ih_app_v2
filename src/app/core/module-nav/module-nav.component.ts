import { Component, OnInit } from '@angular/core';
import { ResourceService } from '../resource.service';
import { Subscription } from 'rxjs';
import { ModuleNavLink } from '../../interfaces/core';

@Component({
  selector: 'app-module-nav',
  templateUrl: './module-nav.component.html',
  styleUrls: ['./module-nav.component.scss'],
})
export class ModuleNavComponent implements OnInit {
  moduleName$: string;
  navLinks$: ModuleNavLink[];
  showModuleNav$ = false;

  private moduleState$: Subscription;

  constructor(
    private resourceService: ResourceService,
  ) {
  }

  ngOnInit(): void {
    this.moduleState$ = this.resourceService.moduleNavConfigState$.subscribe((config) => {
      if (config) {
        this.moduleName$ = config.name;
        this.navLinks$ = config.navLinks;
        this.showModuleNav$ = true;
      } else {
        this.showModuleNav$ = false;
      }
    });
  }

}
