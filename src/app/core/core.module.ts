import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '@angular/cdk/layout';
import { Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from './material.module';

import { NavComponent } from './nav/nav.component';
import { LandingComponent } from './landing/landing.component';
import { BaseViewComponent } from './base-view/base-view.component';
import { PageService } from './page.service';
import { ModuleNavComponent } from './module-nav/module-nav.component';

const components = [
  BaseViewComponent,
  NavComponent,
  LandingComponent,
  ModuleNavComponent,
];

@NgModule({
  declarations: components,
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    RouterModule,
    MaterialModule,
    LayoutModule,
  ],
  exports: [
    [components],
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [
    Title,
    PageService,
  ]
})
export class CoreModule {
}
