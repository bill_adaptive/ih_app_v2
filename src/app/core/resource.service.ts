import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ModuleNavConfig } from '../interfaces/core';

@Injectable({
  providedIn: 'root',
})
export class ResourceService {
  private moduleNavConfigSubject$ = new BehaviorSubject<ModuleNavConfig>(null);
  moduleNavConfigState$ = this.moduleNavConfigSubject$.asObservable();

  constructor() {
  }

  setModuleNavConfig(moduleNavConfig: ModuleNavConfig): void {
    this.moduleNavConfigSubject$.next(moduleNavConfig);
  }

}
