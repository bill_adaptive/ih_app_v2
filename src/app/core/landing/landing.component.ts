import { Component, OnInit } from '@angular/core';
import { ResourceService } from '../resource.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit {

  constructor(
    private resourceService: ResourceService,
  ) {
  }

  ngOnInit(): void {
    this.resourceService.setModuleNavConfig(null);

    console.log(this);
  }

}
