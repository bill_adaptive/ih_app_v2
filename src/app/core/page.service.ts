import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as _ from 'lodash';
import { environment } from '../../environments/environment';
import { Title } from '@angular/platform-browser';

@Injectable({ providedIn: 'root' })
export class PageService {
  isLoading$: BehaviorSubject<boolean> = new BehaviorSubject(true);
  appName = environment.appName;
  appTitle$: string[] = [];
  baseTitle$ = environment.appName;
  private readonly lowerCase$ = true;
  private readonly titleDivider$ = ' | ';

  constructor(
    private titleSvc: Title,
  ) {
  }

  setAppTitle(s: string | string[]): void {
    s = _.compact(_.castArray(s));
    let appTitle = _.concat(s, this.appName).join(this.titleDivider$);

    if (this.lowerCase$) {
      appTitle = appTitle.toLowerCase();
    }

    this.titleSvc.setTitle(appTitle);
  }

  get pageTitle(): string {
    return _.concat(this.appTitle$, this.baseTitle$).join(' | ');
  }

  get appTitle(): string {
    return this.appTitle$.join(' | ');
  }
}
