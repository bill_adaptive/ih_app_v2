import { Component, Input, OnInit } from '@angular/core';
import { PageService } from '../page.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  @Input() orientation: 'vertical' | 'horizontal' = 'vertical';
  @Input() navLinks: any[] = [
    {
      path: 'home',
      label: 'Home',
    },
    {
      path: 'form',
      label: 'Forms',
    },
    {
      path: 'report',
      label: 'Reports',
    },
  ];
  activeNavLink$: string;

  constructor(
    private pageService: PageService,
  ) {
  }

  ngOnInit(): void {
    // console.log(this);
  }

  setActiveNav(navLink): void {
    this.activeNavLink$ = navLink.label;
  }
}
