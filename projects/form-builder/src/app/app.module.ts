import { BrowserModule } from '@angular/platform-browser';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { CoreModule } from '../../../../src/app/core/core.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormBuilderBaseComponent } from './form-builder-base/form-builder-base.component';
import { View1Component } from './view1/view1.component';
import { View2Component } from './view2/view2.component';

const providers = [];

@NgModule({
  declarations: [
    AppComponent,
    FormBuilderBaseComponent,
    View1Component,
    View2Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
  ],
  providers,
  bootstrap: [AppComponent],
})
export class AppModule {
}

@NgModule({})
export class FormBuilderModule {
  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: AppModule,
      providers,
    };
  }
}
