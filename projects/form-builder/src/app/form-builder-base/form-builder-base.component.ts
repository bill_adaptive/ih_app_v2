import { Component, OnInit } from '@angular/core';
import { ResourceService } from '../../../../../src/app/core/resource.service';
import { ModuleNavConfig, ModuleNavLink } from '../../../../../src/app/interfaces/core';

@Component({
  selector: 'app-form-builder-base',
  templateUrl: './form-builder-base.component.html',
  styleUrls: ['./form-builder-base.component.scss'],
})
export class FormBuilderBaseComponent implements OnInit {
  navLinks: ModuleNavLink[] = [
    {
      path: 'form/view1',
      label: 'View 1',
    },
    {
      path: 'form/view2',
      label: 'View 2',
    },
  ];

  constructor(
    private resourceService: ResourceService,
  ) {
  }

  ngOnInit(): void {
    const moduleConfig: ModuleNavConfig = {
      name: 'Form Builder',
      navLinks: this.navLinks,
    };
    this.resourceService.setModuleNavConfig(moduleConfig);

    console.log(this);
  }

}
