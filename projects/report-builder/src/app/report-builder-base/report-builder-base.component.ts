import { Component, OnInit } from '@angular/core';
import { ResourceService } from '../../../../../src/app/core/resource.service';
import { ModuleNavConfig } from '../../../../../src/app/interfaces/core';

@Component({
  selector: 'app-report-builder-base',
  templateUrl: './report-builder-base.component.html',
  styleUrls: ['./report-builder-base.component.scss'],
})
export class ReportBuilderBaseComponent implements OnInit {
  navLinks = [
    {
      path: 'report/view1',
      label: 'View 1',
    },
    {
      path: 'report/view2',
      label: 'View 2',
    },
  ];

  constructor(
    private resourceService: ResourceService,
  ) {
  }

  ngOnInit(): void {
    const moduleConfig: ModuleNavConfig = {
      name: 'Report Builder',
      navLinks: this.navLinks,
    };
    this.resourceService.setModuleNavConfig(moduleConfig);

    console.log(this);
  }
}
