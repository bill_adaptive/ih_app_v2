import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportBuilderBaseComponent } from './report-builder-base/report-builder-base.component';
import { View1Component } from './view1/view1.component';
import { View2Component } from './view2/view2.component';
import { BaseViewComponent } from '../../../../src/app/core/base-view/base-view.component';

const routes: Routes = [
  {
    path: '',
    component: BaseViewComponent,
    children: [
      {
        path: 'report',
        component: ReportBuilderBaseComponent,
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'view1' },
          { path: 'view1', component: View1Component },
          { path: 'view2', component: View2Component },
        ],
      },
    ]
  },
  // {
  //   path: 'report',
  //   component: ReportBuilderBaseComponent,
  //   children: [
  //     { path: '', pathMatch: 'full', redirectTo: 'view1' },
  //     { path: 'view1', component: View1Component },
  //     { path: 'view2', component: View2Component },
  //   ],
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
